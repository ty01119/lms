<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Main Lang - English
*
* Author: Usman Sher
* 		  uskhan099@gmail.com
*         @usmansher
*
* Description:  English language file for Main views
*
*/
$lang['settings_name'] = "Название института";
$lang['settings_address'] = "институт Адрес";
$lang['settings_email'] = "институт E-mail";
$lang['settings_phone'] = "институт телефона";
$lang['settings_logo'] = "логотип";
$lang['settings_favicon'] = "Favicon";
$lang['settings_fine'] = "штраф";
$lang['settings_fine_conf'] = "Выпуск Config";
$lang['settings_fine_limit_book'] = "Выпуск Limit - Книги";
$lang['settings_fine_limit_days'] = "Выпуск Limit - Дни";
$lang['settings_fine_memwise'] = "Члены Мудрого";
$lang['settings_fine_syswide'] = "Общесистемный";
$lang['settings_currency'] = "валюта";
$lang['settings_tnc'] = "Правила и условия";
$lang['settings_save'] = "Сохранить";
$lang['settings_cancel'] = "Отмена";
$lang['settings_title'] = "общие настройки";
$lang['login_title'] = "Авторизоваться";
$lang['login_title_subheading'] = "Войдите на сайт, чтобы начать сеанс";
$lang['login_email'] = "Эл. адрес";
$lang['login_password'] = "пароль";
$lang['remember_me'] = "Запомни меня";
$lang['login_in'] = "Вход Me In";
$lang['export_to_excel'] = "Экспорт в Excel";
$lang['export_to_pdf'] = "Экспорт в PDF";
$lang['csv_format_error'] = "Формат CSV является неправильным.";
$lang['email_heading'] = "E-mail / SMS Шаблон";
$lang['save_template_success'] = "E-mail / SMS Шаблон успешно сохранен";
$lang['save_template_error'] = "Ошибка при сохранении E-mail / SMS шаблона";
$lang['mail_message'] = "Шаблон сообщения электронной почты";
$lang['short_tags'] = "Короткие теги";
$lang['smtp_user'] = "SMTP Пользователь";
$lang['smtp_host'] = "SMTP хост";
$lang['smtp_port'] = "Порт SMTP";
$lang['smtp_pass'] = "SMTP Pass";
$lang['email_sent_error'] = "E-mail / SMS не отправлено";
$lang['lang_updated'] = "Язык успешно обновлен";
$lang['lang_not_updated'] = "Ошибка при updatng Язык";
$lang['sms_sent_success'] = "SMS (ы) успешно отправлено";
$lang['toggle_alignment'] = "Переключить Выравнивание";
$lang['remind_due'] = "напоминать";
$lang['reminder_type'] = "Тип";
$lang['subject_reminder'] = "Предмет";
$lang['reminder_message'] = "Сообщение";
$lang['select_type'] = "Пожалуйста, выберите тип загрузки шаблона";
$lang['send_label'] = "послать";
$lang['email_label'] = "Эл. почта";
$lang['sms_label'] = "смс";
$lang['notify_delayed_no_days_limit_toggle'] = "Сообщите членам, если No-Limit";
$lang['issue_limit_days_extendable'] = "Впритык Выдвижная";
$lang['settings_yes'] = "да";
$lang['settings_no'] = "Нет";
$lang['book_issue_limit_exceed_books'] = 'Лимит выпуска Превышен для книг. Предыдущие невозвращенные книги:%1$s, выпуск книги - Лимит:%2$s';

$lang['print_barcode_title'] = 'Печать штрих-кодов (ы)';
$lang['print_label'] = 'Распечатать';
$lang['add_item'] = 'Добавить элемент';
$lang['book_title_label'] = 'Заголовок книги';
$lang['isbn_label'] = 'ISBN';
$lang['qty_label'] = 'Кол-во.';
$lang['style'] = 'Стиль';
$lang['30_per_sheet'] = '30 на одном листе (2,625 "х 1")';
$lang['20_per_sheet'] = '20 на одном листе бумаги (4 "х 1")';
$lang['14_per_sheet'] = '14 на одном листе бумаги (4 "х 1,33")';
$lang['10_per_sheet'] = '10 на одном листе бумаги (4 "х 2")';
$lang['40_per_sheet'] = '40 на одном листе (а4) (1,799 "х 1,003")';
$lang['24_per_sheet'] = '24 на одном листе (а4) (2,48 "х 1,334")';
$lang['18_per_sheet'] = '18 на одном листе (а4) (2,5 "х 1,835")';
$lang['12_per_sheet'] = '12 на одном листе (а4) (2,5 "х 2,834")';
$lang['continuous_feed'] = 'Непрерывная подача';
$lang['print_barcode_heading'] = 'Вы можете посетить %s и %s, чтобы добавить книги в этот список печати.';
$lang['barcode_tip'] = 'Пожалуйста, не забудьте установить правильный размер страницы и поля для вашего принтера. Вы можете установить справа и снизу до 0 в то время как левый и верхнее поле может быть скорректирована в соответствии с вашими потребностями.';
$lang['width'] = 'Ширина';
$lang['height'] = 'Высота';
$lang['inches'] = 'дюймов';
$lang['orientation'] = 'ориентация';
$lang['portrait'] = 'Портрет';
$lang['landscape'] = 'Пейзаж';
$lang['print'] = 'Распечатать';
$lang['site_name'] = 'Название сайта';
$lang['product_name'] = 'Название книги';
$lang['price'] = 'Цена';
$lang['category'] = 'категория';
$lang['author'] = 'автор';
$lang['product_image'] = 'Обложка книги Изображение';
$lang['update'] = 'Обновить';
$lang['reset'] = 'Сброс';
$lang['no_book_selected'] = 'Ни одна книга не выбрано';
$lang['no_book_found'] = 'Ни одна книга не найдены';
$lang['unexpected_value'] = 'Неожиданный Значение';
$lang['product_added_to_list'] = "Книга добавлены в портфель";
$lang['books_custom_fields'] = "Книги - Пользовательские поля";

//Menu
$lang['menu_home'] = "Главная";
$lang['menu_book_title'] = "книги";
$lang['menu_book_list'] = "Список";
$lang['menu_add_book'] = "Добавить книгу";
$lang['menu_authors'] = "Авторы";
$lang['menu_categories'] = "категории";
$lang['menu_importbooks'] = "Импорт CSV";
$lang['menu_labelprint'] = "этикетки для печати";
$lang['menu_circulation'] = "циркуляция";
$lang['menu_issue_book'] = "Выпуск книги (ы)";
$lang['menu_return_book'] = "Возвращение книги (ы)";
$lang['menu_borrowed_books'] = "Заемные Книги";
$lang['menu_lost_books'] = "Потерянные книги";
$lang['menu_returned_book'] = "Возвращается книги";
$lang['menu_delated_title'] = "уведомить члена";
$lang['menu_notify_delayed'] = "Поставить в известность";
$lang['menu_send_templates'] = "E-mail / SMS Шаблоны";
$lang['menu_admin_panel'] = "Панель администратора";
$lang['menu_admin_users'] = "пользователей";
$lang['menu_create_users'] = "Создать пользователя";
$lang['menu_member_types'] = "Тип Участник";
$lang['menu_occupations'] = "Профессии";
$lang['menu_settings_title'] = "настройки";
$lang['menu_settings'] = "настройки";
$lang['menu_db_version'] = "Версии базы данных";
$lang['menu_smsconfig'] = "SMS Config";
$lang['menu_mybooks_title'] = "Мои книги";
$lang['menu_mybooks'] = "Мои книги";
$lang['menu_circulation_history'] = "Тираж История";
$lang['menu_reports_title'] = "Отчеты";
$lang['menu_qinventory'] = "Быстрый инвентаризации";