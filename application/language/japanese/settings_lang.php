<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Main Lang - English
*
* Author: Usman Sher
* 		  uskhan099@gmail.com
*         @usmansher
*
* Description:  English language file for Main views
*
*/

$lang['settings_name'] = "研究所の名前";
$lang['settings_address'] = "研究所住所";
$lang['settings_email'] = "研究所のメール";
$lang['settings_phone'] = "研究所の電話番号";
$lang['settings_logo'] = "ロゴ";
$lang['settings_favicon'] = "ファビコン";
$lang['settings_fine'] = "ファイン";
$lang['settings_fine_conf'] = "問題コンフィグ";
$lang['settings_fine_limit_book'] = "問題の制限 - 書籍";
$lang['settings_fine_limit_days'] = "問題の制限 - 日";
$lang['settings_fine_memwise'] = "メンバーワイズ";
$lang['settings_fine_syswide'] = "システム全体の";
$lang['settings_currency'] = "通貨";
$lang['settings_tnc'] = "利用規約";
$lang['settings_save'] = "セーブ";
$lang['settings_cancel'] = "キャンセル";
$lang['settings_title'] = "一般設定";
$lang['login_title'] = "ログイン";
$lang['login_title_subheading'] = "あなたのセッションを開始するには、ログインしてください";
$lang['login_email'] = "Eメール";
$lang['login_password'] = "パスワード";
$lang['remember_me'] = "私を覚えてますか";
$lang['login_in'] = "ミーログイン";
$lang['export_to_excel'] = "Excelへのエクスポート";
$lang['export_to_pdf'] = "PDFへのエクスポート";
$lang['csv_format_error'] = "CSV形式は間違っています。";
$lang['email_heading'] = "メール/ SMSテンプレート";
$lang['save_template_success'] = "メール/ SMSテンプレートが保存されました";
$lang['save_template_error'] = "保存エラーメール/ SMSテンプレート";
$lang['mail_message'] = "メールテンプレート";
$lang['short_tags'] = "ショートタグ";
$lang['smtp_user'] = "SMTPユーザー";
$lang['smtp_host'] = "SMTPホスト";
$lang['smtp_port'] = "SMTPポート";
$lang['smtp_pass'] = "SMTPパス";
$lang['email_sent_error'] = "メール/ SMSが送信されません";
$lang['lang_updated'] = "言語が正常に更新されました";
$lang['lang_not_updated'] = "更新エラーの言語";
$lang['sms_sent_success'] = "SMS（s）は正常に送信され";
$lang['toggle_alignment'] = "トグルアライメント";
$lang['remind_due'] = "リマインド";
$lang['reminder_type'] = "タイプ";
$lang['subject_reminder'] = "テーマ";
$lang['reminder_message'] = "メッセージ";
$lang['select_type'] = "テンプレートをロードするタイプを選択してください";
$lang['send_label'] = "送信";
$lang['email_label'] = "Eメール";
$lang['sms_label'] = "SMS";
$lang['notify_delayed_no_days_limit_toggle'] = "ノーリミットがあればメンバーに通知";
$lang['issue_limit_days_extendable'] = "期日拡張可能";
$lang['settings_yes'] = "はい";
$lang['settings_no'] = "いいえ";
$lang['book_issue_limit_exceed_books'] = '発行限度はブックのために超えています。前の未返還洋書：％1$s の、問題ブック - 制限：％2$s の';

$lang['print_barcode_title'] = '印刷バーコード（複数可）';
$lang['print_label'] = '印刷';
$lang['add_item'] = 'アイテムを追加';
$lang['book_title_label'] = 'マニュアルのタイトル';
$lang['qty_label'] = '数量。';
$lang['style'] = 'スタイル';
$lang['30_per_sheet'] = 'シートあたり30（2.625"×1"）';
$lang['20_per_sheet'] = 'シート当たり20（4"×1"）';
$lang['14_per_sheet'] = 'シート当たり14（4"×1.33"）';
$lang['10_per_sheet'] = 'シート当たり10（4"×2"）';
$lang['40_per_sheet'] = 'シートあたり40（A4）（1.799"×1.003"）';
$lang['24_per_sheet'] = 'シートあたり24（A4）（2.48"×1.334"）';
$lang['18_per_sheet'] = 'シートあたり18（A4）（2.5"×1.835"）';
$lang['12_per_sheet'] = 'シートあたり12（A4）（2.5"×2.834"）';
$lang['continuous_feed'] = '連続供給';
$lang['print_barcode_heading'] = 'あなたが訪問することができます %s および %s この印刷リストに本を追加します。';
$lang['barcode_tip'] = 'お使いのプリンタの正しいページサイズとマージンを設定することを忘れないでください。左と上余白があなたの必要性に応じて調整することができながら、あなたは0に右と下に設定することができます。';
$lang['width'] = '幅';
$lang['height'] = '高さ';
$lang['inches'] = 'インチ';
$lang['orientation'] = 'オリエンテーション';
$lang['portrait'] = '肖像画';
$lang['landscape'] = '風景';
$lang['print'] = '印刷';
$lang['site_name'] = 'サイト名';
$lang['product_name'] = '書籍名';
$lang['price'] = '価格';
$lang['category'] = 'カテゴリー';
$lang['author'] = '著者';
$lang['product_image'] = 'ブックカバーイメージ';
$lang['update'] = '更新';
$lang['reset'] = 'リセット';
$lang['no_book_selected'] = 'いいえブックが選択されていません';
$lang['no_book_found'] = 'いいえブックが見つかりません';
$lang['unexpected_value'] = '予期しない値';
$lang['product_added_to_list'] = "本がリストに追加しました";

$lang['books_custom_fields'] = "書籍 - カスタムフィールド";


$lang['menu_home'] = "ホーム";
$lang['menu_book_title'] = "図書";
$lang['menu_book_list'] = "リスト";
$lang['menu_add_book'] = "ブックを追加します。";
$lang['menu_authors'] = "著者";
$lang['menu_categories'] = "カテゴリー";
$lang['menu_importbooks'] = "CSVのインポート";
$lang['menu_labelprint'] = "ラベル/バーコードを印刷";
$lang['menu_circulation'] = "サーキュレーション";
$lang['menu_issue_book'] = "問題ブック（複数可）";
$lang['menu_return_book'] = "戻り帳（複数可）";
$lang['menu_borrowed_books'] = "借りた本";
$lang['menu_lost_books'] = "ロストブックス";
$lang['menu_returned_book'] = "返さブック";
$lang['menu_delated_title'] = "遅延メンバーに通知";
$lang['menu_notify_delayed'] = "通知します";
$lang['menu_send_templates'] = "メール/ SMSのテンプレート";
$lang['menu_admin_panel'] = "管理パネル";
$lang['menu_admin_users'] = "ユーザー";
$lang['menu_create_users'] = "ユーザーの作成";
$lang['menu_member_types'] = "メンバー型";
$lang['menu_occupations'] = "職業";
$lang['menu_settings_title'] = "設定";
$lang['menu_settings'] = "設定";
$lang['menu_db_version'] = "データベースのバージョン";
$lang['menu_smsconfig'] = "SMSコンフィグ";
$lang['menu_mybooks_title'] = "私の本";
$lang['menu_mybooks'] = "私の本";
$lang['menu_circulation_history'] = "循環歴史";
$lang['menu_reports_title'] = "レポート";
$lang['menu_qinventory'] = "クイックインベントリ";