<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Home extends MY_Controller {

	public function index()
	{
		$this->load->model('home_model');
		$this->data['books'] = $this->home_model->getBookList();
		$this->render('home', $this->data);
	}
	

	
}
