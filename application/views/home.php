 <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> <?= lang('book_list'); ?>
                    <small></small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
        <?php if ($books): ?>
        		<?php $count = 0; foreach($books as $book):
        
		        	if($count % 4 == 0): ?>
		        		 </div><div class="row">
		        	<?php endif; ?>
		            <div class="col-md-3 portfolio-item">
		                <a href="#">
		                    <img style="height: 200px" class="img-responsive" src="<?= base_url(); ?>assets/uploads/book_covers/<?= $book->image; ?>" alt="A">
		                </a>
		                <h3>
		                    <a href="#" alt="<?= $book->book_title ?>"><?= substr($book->book_title,  0, 20); ?></a>
		                </h3>
		                <p><?= substr($book->description, 0, 200); ?></p>
		            </div>
		          
		        <?php $count += 1; endforeach; ?>
        <?php else: ?>
        	<h1 class="text-center">No Books in Database :D</h1>
        <?php endif;?>
        </div>
        <!-- /.row -->


       


      
    </div>
    <!-- /.container -->