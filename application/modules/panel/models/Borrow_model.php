<?php 
Class Borrow_model extends CI_Model
{
    
    function insertBorrow($data = null, $books = null) 
    { 
        // echo '<pre>';
        // print_r($data);
        // print_r($book_authors);
        // print_r($book_categories);
        // echo '</pre>';

        $adminId=$_SESSION['user_id'];
        $sql="select location from users where id=$adminId";
        $sqlData = $this->db->query($sql);
        if ($sqlData->num_rows() > 0) {
            $sqlDataRow = $sqlData->row();
            $location = $sqlDataRow->location;
        }
        if ($data AND $books) {
            if($this->db->insert('borrow', $data)) {
                $borrow_id = $this->db->insert_id();
                foreach ($books as $book) {
                    $bookCode = $book->code;
                    $bookId = $book->row->id;
                    $sql="select book_location from books where id=" . $bookId;                 
                    $bookLocationRES = $this->db->query($sql);
                    if($bookLocationRES->num_rows() > 0){
                        $bookLocationObj = $bookLocationRES->row();
                        $bookLocationString = $bookLocationObj->book_location;
                    }
                    $bookLocationRaw = explode(";", $bookLocationString);
                    $bookLocations = array();
                    foreach ($bookLocationRaw as $value) {
                        $temp = explode(":", $value);
                        $bookLocation['code'] = $temp[0];
                        $vars = explode(",", $temp[1]);
                        $bookLocation['ownership'] = $vars[0];
                        $bookLocation['location'] = $vars[1];
                        $bookLocation['available'] = $vars[2];
                        if(strpos($bookLocation['code'], $bookCode) !== false){
                            $bookCode = $bookLocation['code'];
                            $bookLocation['available'] = '0';
                        }
                        array_push($bookLocations, $bookLocation);
                    }
                    $bookLocationString = "";
                    foreach ($bookLocations as $bookLocation) {
                        $bookLocationString .= $bookLocation['code'] . ":" . $bookLocation['ownership'] . "," . $bookLocation['location'] . "," . $bookLocation['available'] . ";";
                    }
                    $bookLocationString = trim($bookLocationString, ";");
                    $this->db->update("books", array("book_location" => $bookLocationString), array("id" => $bookId));
                    $return = $this->db->insert('borrowdetails', array('book_id' => $bookId, 'borrow_id' => $borrow_id, 'borrow_status' => 'pending', 'book_code' => $bookCode));
                }
            }
        }
       
        return False;

        // return $this->db->affected_rows() >= 1 ? TRUE : FALSE; 
    }
    public function GetBorrowDetailsByID($id)
    {

        $this->db->select('borrow.borrow_id as id, book_title as title, CONCAT(first_name, " ", last_name) as name, date_borrow as date_borrow, due_date as due_date, date_return as date_return, borrow_status as status, email, phone, price, books.id as book_id, member_types.issue_limit_day, book_code');
        $this->db->where('borrowdetails.borrow_details_id = '.$id);
        $this->db->join('users', 'borrow.member_id = users.id', 'left');
        $this->db->join('borrowdetails', 'borrow.borrow_id = borrowdetails.borrow_id', 'left');
        $this->db->join('books', ' borrowdetails.book_id =  books.id ', 'left');
        $this->db->join('member_types', ' member_types.type_id =  users.type_id ', 'left');
        $q = $this->db->get('borrow');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
     public function GetBorrowDetailsByMemberID($id)
    {

        $this->db->select('borrow.borrow_id as id, book_title as title, CONCAT(first_name, " ", last_name) as name, date_borrow as date_borrow, due_date as due_date, date_return as date_return, borrow_status as status, email, phone, price, books.id as book_id, books.isbn as isbn, borrowdetails.borrow_details_id as borrowdetails_tid');
        $this->db->where('users.member_unique_id', $id);
        $this->db->where('borrowdetails.borrow_status = "pending"');
        $this->db->join('users', 'borrow.member_id = users.id', 'left');
        $this->db->join('borrowdetails', 'borrow.borrow_id = borrowdetails.borrow_id', 'left');
        $this->db->join('books', ' borrowdetails.book_id =  books.id ', 'left');
        $q = $this->db->get('borrow');
        $data = array();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return ($data);
        }
        return FALSE;
    }
    public function getCountLimitBooks($id = NULL)
    {

        $this->db->select('COUNT(borrowdetails.borrow_details_id) as count');
        $this->db->where('borrow.member_id', $id);
        $this->db->where('borrowdetails.borrow_status = "pending"');
        $this->db->join('borrowdetails', 'borrow.borrow_id = borrowdetails.borrow_id', 'left');
        $q = $this->db->get('borrow');
        if ($q->num_rows() > 0) {
            return ($q->row()->count);
        }
        return FALSE;
    }

    function update_status($where, $data) 
    { 
        echo '<pre>';
        var_dump($data);
        var_dump($where);
        echo '</pre>';
        
        $adminId=$_SESSION['user_id'];
        $sql="select location from users where id=$adminId";
        $sqlData = $this->db->query($sql);
        if ($sqlData->num_rows() > 0) {
            $sqlDataRow = $sqlData->row();
            $location = $sqlDataRow->location;
        }
        if ($where) {
            if($data['borrow_status'] == "returned"){
                $bookId = $where['book_id'];
                $bookCode = $data['book_code'];

                $sql="select book_location from books where id=" . $bookId;                 
                $bookLocationRES = $this->db->query($sql);
                if($bookLocationRES->num_rows() > 0){
                    $bookLocationObj = $bookLocationRES->row();
                    $bookLocationString = $bookLocationObj->book_location;
                }
                $bookLocationRaw = explode(";", $bookLocationString);
                echo "bookLocationRaw: <pre>"; var_dump($bookLocationRaw); echo "</pre>";
                $bookLocations = array();
                foreach ($bookLocationRaw as $value) {
                    $temp = explode(":", $value);
                    $bookLocation['code'] = $temp[0];
                    $vars = explode(",", $temp[1]);
                    $bookLocation['ownership'] = $vars[0];
                    $bookLocation['location'] = $vars[1];
                    $bookLocation['available'] = $vars[2];
                    if(strpos($bookLocation['code'], $bookCode) !== false){
                        $bookLocation['available'] = '1';
                        $bookLocation['location'] = $location;
                    }
                    array_push($bookLocations, $bookLocation);
                }
                echo "bookLocations: <pre>"; var_dump($bookLocations); echo "</pre>";
                $bookLocationString = "";
                foreach ($bookLocations as $bookLocation) {
                    $bookLocationString .= $bookLocation['code'] . ":" . $bookLocation['ownership'] . "," . $bookLocation['location'] . "," . $bookLocation['available'] . ";";
                }
                $bookLocationString = trim($bookLocationString, ";");
                $this->db->update("books", array("book_location" => $bookLocationString), array("id" => $bookId));
            }            
            $this->db->where($where);
            $this->db->update('borrowdetails', $data);
            
            return True;
            
        }
        return False;

        // return $this->db->affected_rows() >= 1 ? TRUE : FALSE; 
    }

    function send_emails($due_ids = NULL, $type = 'email')
    {
        $dues = array();

        foreach ($due_ids as $due_id) {
            // echo $due_id;
            $this->db->select('book_title as title, users.email as email, CONCAT(first_name, " ", last_name) as name, date_borrow as date_borrow, due_date as due_date, users.phone as phone');
            $this->db->join('users', 'borrow.member_id = users.id', 'left');
            $this->db->join('borrowdetails', 'borrow.borrow_id = borrowdetails.borrow_id', 'left');
            $this->db->join('books', ' borrowdetails.book_id =  books.id ', 'left');
            $this->db->where('borrow_details_id', $due_id);
            $q = $this->db->get('borrow');
            if ($q->num_rows() > 0) {
                $results = TRUE;
                if ($type == 'email') {
                    $id = $q->row()->email;
                }else{
                    $id = $q->row()->phone;
                }
                $dues[$id][] = $q->row();
            }
        }
        if ($results) {
            return ($dues);
        }else{
            return FALSE;
        }

    }
    public function getIssueConfigByMemberTypeID($id)
    {

        $this->db->select('issue_limit_books, issue_limit_day');
        $this->db->where('type_id', $id);
        $q = $this->db->get('member_types');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getUserTypeIDByMemberID($id)
    {

        $this->db->select('type_id');
        $this->db->where('id', $id);
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    
  
}
?>