<?php

Class Home_model extends CI_Model
{
	public function getBookList($limit = 50)
    {
        $q = $this->db->query("SELECT *, 
            book_copies - (SELECT Count(borrowdetails.borrow_id) FROM  borrowdetails WHERE books.id = borrowdetails.book_id AND borrow_status = 'lost') as total_quantity, 
            book_copies - (SELECT Count(borrowdetails.borrow_id) FROM borrowdetails WHERE books.id = borrowdetails.book_id AND borrow_status = 'pending') as available
        FROM books
        ORDER BY isbn
        LIMIT $limit");


        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    public function getBookByID($id)
    {
        $q = $this->db->get_where('books', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    
}

?>