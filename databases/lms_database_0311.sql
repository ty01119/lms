-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: lms
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `author_name` (`author_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (2,'Blake Masters'),(1,'Peter Thiel');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_authors`
--

DROP TABLE IF EXISTS `book_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_authors` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `book_id` int(100) NOT NULL,
  `author_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_authors`
--

LOCK TABLES `book_authors` WRITE;
/*!40000 ALTER TABLE `book_authors` DISABLE KEYS */;
INSERT INTO `book_authors` VALUES (20,5,1);
/*!40000 ALTER TABLE `book_authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_categories`
--

DROP TABLE IF EXISTS `book_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_categories` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `book_id` int(100) NOT NULL,
  `category_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_categories`
--

LOCK TABLES `book_categories` WRITE;
/*!40000 ALTER TABLE `book_categories` DISABLE KEYS */;
INSERT INTO `book_categories` VALUES (20,5,1);
/*!40000 ALTER TABLE `book_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(100) NOT NULL,
  `book_copies` int(50) NOT NULL,
  `book_pub` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `digital_file` varchar(100) NOT NULL,
  `isbn` varchar(50) CHARACTER SET utf8 NOT NULL,
  `isbn_13` varchar(13) NOT NULL,
  `price` int(40) NOT NULL,
  `copyright_year` varchar(11) NOT NULL,
  `date_receive` date NOT NULL,
  `date_added` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  `custom_fields` longtext NOT NULL,
  `description` text,
  `book_location` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn` (`isbn`),
  UNIQUE KEY `isbn_13` (`isbn_13`),
  KEY `isbn_2` (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (5,'123',5,'123','no_image.png','','ISBN123','123',123,'2009','2017-03-01','2017-03-11 23:16:43','','{\"6e6f746573\":\"123\"}','123','aaa:1,1,1;bbb:1,1,1;ccc:3,1,1;ddd:5,5,1;eee:4,3,1');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrow`
--

DROP TABLE IF EXISTS `borrow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrow` (
  `borrow_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(50) NOT NULL,
  `date_borrow` varchar(100) NOT NULL,
  `due_date` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`borrow_id`),
  KEY `borrowerid` (`member_id`),
  KEY `borrowid` (`borrow_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrow`
--

LOCK TABLES `borrow` WRITE;
/*!40000 ALTER TABLE `borrow` DISABLE KEYS */;
INSERT INTO `borrow` VALUES (1,2,'2017-03-11 22:10:03','2017-03-21');
/*!40000 ALTER TABLE `borrow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrowdetails`
--

DROP TABLE IF EXISTS `borrowdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrowdetails` (
  `borrow_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `borrow_id` int(11) NOT NULL,
  `borrow_status` enum('pending','returned','lost','') NOT NULL,
  `fine` decimal(6,2) DEFAULT NULL,
  `price_lost_book` decimal(6,2) DEFAULT NULL,
  `date_return` datetime DEFAULT NULL,
  `book_code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`borrow_details_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrowdetails`
--

LOCK TABLES `borrowdetails` WRITE;
/*!40000 ALTER TABLE `borrowdetails` DISABLE KEYS */;
INSERT INTO `borrowdetails` VALUES (25,5,1,'returned',NULL,NULL,'2017-03-11 22:10:25','aaa');
/*!40000 ALTER TABLE `borrowdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id` (`id`),
  UNIQUE KEY `category_name` (`category_name`),
  KEY `classid` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Political Science');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('050d7ba5837dddd352ff11f2139fb2edc22c6283','::1',1489225894,'__ci_last_regenerate|i:1489225893;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('07edf98ddde48db3a765f727b88d24a91721b304','::1',1489203076,'__ci_last_regenerate|i:1489203076;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('0857ebc34214f2e66ec7ca8058729d7200c932c4','::1',1489221619,'__ci_last_regenerate|i:1489221358;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('0d3b86ca0fb1fedbbca95065e663c255bc1b898a','::1',1489203721,'__ci_last_regenerate|i:1489203462;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('111f287533570908cc6d6585eff4a7be6a88e528','::1',1489200471,'__ci_last_regenerate|i:1489200220;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('11835ffa7351f27e9dbeb05651d11b2a0ab340cf','::1',1489191024,'__ci_last_regenerate|i:1489190776;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('18a37b964b4b7565e8843883352cdaeb4ff01b47','::1',1489194087,'__ci_last_regenerate|i:1489193819;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('21e22db854fe61f32701d7dc3133d42f18552fff','::1',1489223426,'__ci_last_regenerate|i:1489223136;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('2247d6f2c12b95d0d0a3d94f1542b019d96e55fa','::1',1489204036,'__ci_last_regenerate|i:1489203767;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('27309f530ba0d9ca468ea989f44401b4a2bfc556','::1',1489199859,'__ci_last_regenerate|i:1489199548;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('29b3e2589c1f9e05a8f67fe72ac3d418abe2a30e','::1',1489146567,'__ci_last_regenerate|i:1489146553;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489113598\";'),('2d0055b57ffd4f711aa584ecaf3ffb5035b95db9','::1',1489222307,'__ci_last_regenerate|i:1489222206;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('402f2e3e18624ad8470a207b535c4b9b1b97fe77','::1',1489195828,'__ci_last_regenerate|i:1489195811;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('40600752f6b715a14918ae058b64147093809fef','::1',1489195344,'__ci_last_regenerate|i:1489194781;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('44f503a4aafa59b09c8551be82b5a50191673706','::1',1489200474,'__ci_last_regenerate|i:1489200183;'),('454c4d21e799539284c507121f5800e6256be508','::1',1489200910,'__ci_last_regenerate|i:1489200608;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('53fda68e3ff6cc443d3e96a2c7585cbf8d12003c','::1',1489146425,'__ci_last_regenerate|i:1489146161;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489113598\";'),('5488b095f139bffda191225badd4f889f7962e89','::1',1489220063,'__ci_last_regenerate|i:1489219949;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('56f7511611e2b62e14ad1a9dceb5077f258c3f1d','::1',1489147054,'__ci_last_regenerate|i:1489147052;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489146163\";'),('6232db59ab6e24d5b1e2d02ea4d5e0181c0d697a','::1',1489227404,'__ci_last_regenerate|i:1489227361;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('6952d975509ad130df3a43fe2566f493a1f82d52','::1',1489225590,'__ci_last_regenerate|i:1489225496;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('6aacb70d6bcd3c2b2d4d5ec6b938118e5e733958','::1',1489202953,'__ci_last_regenerate|i:1489202657;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('6b45ca4629783f02a87187841b3ebb435b8265f8','::1',1489199319,'__ci_last_regenerate|i:1489199028;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('70db959c7150bfba34ef8d3fd81221435f1b8bea','::1',1489199516,'__ci_last_regenerate|i:1489199426;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('7bcd6b6412108d797188a9a19883d14beafe9ec6','::1',1489225071,'__ci_last_regenerate|i:1489224895;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('7fde9e65b278259396e4eb96d65c5312eef6630a','::1',1489192211,'__ci_last_regenerate|i:1489191997;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('8396ba284372ffb0778a37af56e9ad502b85af21','::1',1489195810,'__ci_last_regenerate|i:1489195510;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('8419bca6b44c7ac22409f9652e0488063672ef19','::1',1489223707,'__ci_last_regenerate|i:1489223453;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('8f65af41037fc213e59cbaf8bc8e05ad339047da','::1',1489222812,'__ci_last_regenerate|i:1489222605;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('923315c536f565060f2a9f2c85298ede14b2c6bc','::1',1489198984,'__ci_last_regenerate|i:1489198671;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('9387af86a6b5f6843ffd8a0bee9d2372d90b2ede','::1',1489204267,'__ci_last_regenerate|i:1489204139;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('98aed0268502220e63cf2ca88ff2bc5fe8696cb6','::1',1489191103,'__ci_last_regenerate|i:1489191083;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('a74b35eee6712e038f85fea6ee0963e607700deb','::1',1489221993,'__ci_last_regenerate|i:1489221661;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('aaa41e5d6741b57f18beadf2a67bce0d08a43cfa','::1',1489202429,'__ci_last_regenerate|i:1489202161;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('ac3937dbd9258a736e36ff9676d74dd2a633247d','::1',1489194515,'__ci_last_regenerate|i:1489194442;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('b579edaf36f8081abbff9d69e89cf1149ebc3e7f','::1',1489202090,'__ci_last_regenerate|i:1489201796;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('bbeaa55f22e04200faf57dacbf73970fe397b25d','::1',1489226598,'__ci_last_regenerate|i:1489226445;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('bfa9f30122563f952bc1fb6d2cb5d434c907fbdc','::1',1489220542,'__ci_last_regenerate|i:1489220313;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('c0cdbc5f68114d5aa1e5b47592a0431096d184ab','::1',1489200205,'__ci_last_regenerate|i:1489199880;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('c38ce91ad14a3179ec95a53cd6449b084f72b618','::1',1489190425,'__ci_last_regenerate|i:1489190418;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('c45859db45c75ad5d64862e0a3537b5a15753c3f','::1',1489197869,'__ci_last_regenerate|i:1489197491;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('c52df1094fe7ad5d3d7600a9a4fdde4580df6e68','::1',1489223957,'__ci_last_regenerate|i:1489223766;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('c6cfe197b9e49c64561dc2c870a764fdb8691b5a','::1',1489200908,'__ci_last_regenerate|i:1489200610;'),('c9620c830268f307f041d3723450395533c9e071','::1',1489194407,'__ci_last_regenerate|i:1489194141;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('cb811cdfdcbca5f58e354f43b23e3ed9484d9fd0','::1',1489147588,'__ci_last_regenerate|i:1489147372;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489146163\";'),('cc213925f61a311d6dbc61d6c9d3c6ce2c993623','::1',1489198200,'__ci_last_regenerate|i:1489197895;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('d56468b07562b15fcba1ccb88af2ced85ab7745d','::1',1489201412,'__ci_last_regenerate|i:1489201112;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('e33710dca316e9942c7e8a00e346d2cab1bc4620','::1',1489200182,'__ci_last_regenerate|i:1489199882;'),('e45c45126a075053c862cf6c5d712c9aa9dd6c4d','::1',1489219850,'__ci_last_regenerate|i:1489219640;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('e54d157ba8958e7f1cde51dd1f7c9508aa4960e8','::1',1489219322,'__ci_last_regenerate|i:1489219147;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";message|s:35:\"You have successfully issued a Book\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('e7a6e346d9d1d5ca0cc2d0622cf36ed210e09385','::1',1489201683,'__ci_last_regenerate|i:1489201465;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('eab373f635f9f5ed7314972f69ce1c8ba50f13da','::1',1489220953,'__ci_last_regenerate|i:1489220706;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('f2d6d9e8532da798c6b772a30824fc5954e61e4b','::1',1489198649,'__ci_last_regenerate|i:1489198203;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('f377dfbe9f5bf5f08a62ad6fba2c9a564fc82324','::1',1489199776,'__ci_last_regenerate|i:1489199544;'),('fafe21d8d1af0cbc969d3dd596885ed1ac919ef1','::1',1489191936,'__ci_last_regenerate|i:1489191643;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('fc7397e1d1cde08e16d76dad275e25bda0330db0','::1',1489227232,'__ci_last_regenerate|i:1489226968;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `type_id` int(5) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES (1,'Class 1',1),(2,'Class 2',1),(3,'Class 3',1),(4,'Class 4',1),(5,'Class 5',1),(6,'Class 6',1),(7,'Class 7',1),(8,'Class 8',1),(9,'Olevel',1),(10,'Alevel',1),(11,'IT',3),(12,'Junior Teacher',2),(13,'Senior Teacher',2),(14,'Accountant',3),(15,'Guest',4);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Sudoer Administrator'),(2,'members','General User'),(3,'admin','Staff');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_types`
--

DROP TABLE IF EXISTS `member_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `borrowertype` varchar(50) DEFAULT NULL,
  `fine` varchar(100) NOT NULL,
  `issue_limit_books` varchar(50) NOT NULL,
  `issue_limit_day` varchar(50) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_types`
--

LOCK TABLES `member_types` WRITE;
/*!40000 ALTER TABLE `member_types` DISABLE KEYS */;
INSERT INTO `member_types` VALUES (1,'Student','','10','10'),(2,'Instructional','','0','0'),(3,'Management','','10','10'),(4,'Guest','','10','10');
/*!40000 ALTER TABLE `member_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `books-index` tinyint(1) DEFAULT '0',
  `books-add` tinyint(1) DEFAULT '0',
  `books-edit` tinyint(1) DEFAULT '0',
  `books-delete` tinyint(1) DEFAULT '0',
  `books-read` tinyint(1) DEFAULT '0',
  `books-getBookDetails` tinyint(1) DEFAULT '0',
  `books-import_csv` tinyint(1) DEFAULT '0',
  `book-print_barcodes` tinyint(1) NOT NULL DEFAULT '0',
  `settings-index` tinyint(1) DEFAULT '0',
  `issued-index` tinyint(1) DEFAULT '0',
  `books-categories` tinyint(1) DEFAULT '0',
  `books-authors` tinyint(1) DEFAULT '0',
  `borrow-index` tinyint(1) DEFAULT '0',
  `borrow-bookreturn` tinyint(1) DEFAULT '0',
  `borrow-borrowed` tinyint(1) DEFAULT '0',
  `settings-sms` tinyint(1) DEFAULT '0',
  `settings-list_db` tinyint(1) DEFAULT '0',
  `settings-backup_db` tinyint(1) DEFAULT '0',
  `settings-restore_db` tinyint(1) DEFAULT '0',
  `settings-remove_db` tinyint(1) DEFAULT '0',
  `auth-index` tinyint(1) DEFAULT '0',
  `auth-create_user` tinyint(1) DEFAULT '0',
  `auth-groups` tinyint(1) DEFAULT '0',
  `auth-edit_group` tinyint(1) DEFAULT '0',
  `auth-member_types` tinyint(1) DEFAULT '0',
  `auth-occupations` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-quick_inventory` tinyint(1) DEFAULT '0',
  `delayed-index` tinyint(1) NOT NULL DEFAULT '0',
  `delayed-due_books_json` tinyint(1) NOT NULL DEFAULT '0',
  `delayed-email_templates` tinyint(1) NOT NULL DEFAULT '0',
  `delayed-notify_delayed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,2,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,1,1,0,0,1,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `section_id` int(50) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(50) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,'A'),(2,'B'),(3,'C'),(4,'D'),(5,'E'),(6,'F'),(7,'G'),(8,'H'),(9,'I'),(10,'J'),(11,'K'),(12,'L'),(13,'M'),(14,'N'),(15,'O'),(16,'P'),(17,'Q'),(18,'R'),(19,'S'),(20,'T'),(21,'U'),(22,'V'),(23,'W'),(24,'X'),(25,'Y'),(26,'Z');
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `issue_conf` int(1) NOT NULL COMMENT 'By Member Types: 2, System-Wide:1',
  `fine` int(10) NOT NULL,
  `issue_limit_days` int(10) NOT NULL,
  `issue_limit_books` int(10) NOT NULL,
  `language` varchar(100) NOT NULL DEFAULT 'english',
  `toggle_rtl` tinyint(1) NOT NULL DEFAULT '0',
  `currency` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `favicon` varchar(40) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `title_small` varchar(10) NOT NULL,
  `terms_conditions` text NOT NULL,
  `books_custom_fields` longtext NOT NULL,
  `smtp_host` varchar(100) NOT NULL,
  `smtp_user` varchar(100) NOT NULL,
  `smtp_pass` varchar(255) NOT NULL,
  `smtp_port` varchar(10) NOT NULL DEFAULT '25',
  `version` decimal(6,2) DEFAULT NULL,
  `issue_limit_days_extendable` tinyint(1) NOT NULL,
  `notify_delayed_no_days_limit_toggle` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'Chinese Library Management System- Multi Support',2,50,10,10,'english',0,'NZD','info@multisupport.co.nz','45906a14e9c1bff095fb72be51a05f1f.png','LOGO.ico','panmure','0278226899','LMS','<ul>\r\n <li><strong>All copyright, trade marks, design rights,</strong> patents and other intellectual property rights (registered and unregistered) in and on LMS Online Services and LMS Content belong to the LMS and/or third parties (which may include you or other users.)</li>\r\n <li>The LMS reserves all of its rights in LMS Content and LMS Online Services. Nothing in the Terms grants you a right or licence to use any trade mark, design right or copyright owned or controlled by the LMS or any other third party except as expressly provided in the Terms.</li>\r\n</ul>','notes','ssl://smtp.gmail.com','2312456789@gmail.com','password','465',2.21,1,0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_settings`
--

DROP TABLE IF EXISTS `sms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_gateway` varchar(100) NOT NULL,
  `auth_id` varchar(100) NOT NULL,
  `auth_token` varchar(255) NOT NULL,
  `api_id` varchar(100) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_settings`
--

LOCK TABLES `sms_settings` WRITE;
/*!40000 ALTER TABLE `sms_settings` DISABLE KEYS */;
INSERT INTO `sms_settings` VALUES (1,'nexmo','2132456','2345','12345','+123245');
/*!40000 ALTER TABLE `sms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `gender` enum('Male','Female','Other','') NOT NULL,
  `member_unique_id` varchar(20) NOT NULL,
  `type_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(2) NOT NULL,
  `image` text NOT NULL,
  `address` text NOT NULL,
  `location` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@multisupport.co.nz',NULL,NULL,NULL,'/JLvbejX0W.RyVBcPWoiPe',1268889823,1489219155,1,'Admin','Admin','ADMIN','0','Male','14849857388887',1,1,'A','no_image.png','afs','1'),(2,'27.252.89.60','test','$2y$08$whiO0zW..3T7xe/k/8BIQeROhxYa/B.4AHUa0YZD7ouUoFqw04sO6',NULL,'test@test.com',NULL,NULL,NULL,NULL,1487388263,NULL,1,'test','test','test','test','Male','148738826220893',1,0,'','no_image.png','','1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,1,1),(2,2,2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-11 23:23:25
