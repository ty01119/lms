-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: lms
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `author_name` (`author_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (2,'Blake Masters'),(1,'Peter Thiel');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_authors`
--

DROP TABLE IF EXISTS `book_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_authors` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `book_id` int(100) NOT NULL,
  `author_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_authors`
--

LOCK TABLES `book_authors` WRITE;
/*!40000 ALTER TABLE `book_authors` DISABLE KEYS */;
INSERT INTO `book_authors` VALUES (21,5,1),(22,6,2);
/*!40000 ALTER TABLE `book_authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_categories`
--

DROP TABLE IF EXISTS `book_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_categories` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `book_id` int(100) NOT NULL,
  `category_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_categories`
--

LOCK TABLES `book_categories` WRITE;
/*!40000 ALTER TABLE `book_categories` DISABLE KEYS */;
INSERT INTO `book_categories` VALUES (21,5,1),(22,6,1);
/*!40000 ALTER TABLE `book_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(100) NOT NULL DEFAULT '',
  `book_copies` int(50) NOT NULL,
  `book_pub` varchar(100) NOT NULL DEFAULT '',
  `image` text NOT NULL,
  `digital_file` varchar(100) NOT NULL DEFAULT '',
  `isbn` varchar(50) NOT NULL,
  `isbn_13` varchar(13) NOT NULL DEFAULT '',
  `price` int(40) NOT NULL,
  `copyright_year` varchar(11) NOT NULL DEFAULT '',
  `date_receive` date NOT NULL,
  `date_added` datetime NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT '',
  `custom_fields` longtext NOT NULL,
  `description` text,
  `book_location` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn` (`isbn`),
  UNIQUE KEY `isbn_13` (`isbn_13`),
  KEY `isbn_2` (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (5,'123',5,'123','no_image.png','','ISBN123','123',123,'2009','2017-03-01','2017-03-11 23:46:49','','{\"6e6f746573\":\"123\"}','123','aaa:1,1,1;bbb:2,2,1;ccc:3,1,1;ddd:5,5,1;eee:4,3,1'),(6,'jkhhhhhh',2,'test','no_image.png','','9787503949265','978756151',200,'2014','2017-03-26','2017-03-12 13:28:05','','{\"6e6f746573\":\"\"}','yfhhfhgfhffhf','E004015017997150:1,1,1;:1,1,1');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrow`
--

DROP TABLE IF EXISTS `borrow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrow` (
  `borrow_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(50) NOT NULL,
  `date_borrow` varchar(100) NOT NULL,
  `due_date` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`borrow_id`),
  KEY `borrowerid` (`member_id`),
  KEY `borrowid` (`borrow_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrow`
--

LOCK TABLES `borrow` WRITE;
/*!40000 ALTER TABLE `borrow` DISABLE KEYS */;
INSERT INTO `borrow` VALUES (1,2,'2017-03-11 22:10:03','2017-03-21'),(2,2,'2017-03-12 11:06:33','2017-03-22'),(3,2,'2017-03-12 13:30:54','2017-03-22'),(4,2,'2017-03-12 13:31:32','2017-03-22'),(5,2,'2017-03-12 13:32:57','2017-03-22');
/*!40000 ALTER TABLE `borrow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrowdetails`
--

DROP TABLE IF EXISTS `borrowdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrowdetails` (
  `borrow_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `borrow_id` int(11) NOT NULL,
  `borrow_status` enum('pending','returned','lost','') NOT NULL DEFAULT '',
  `fine` decimal(6,2) DEFAULT NULL,
  `price_lost_book` decimal(6,2) DEFAULT NULL,
  `date_return` datetime DEFAULT NULL,
  `book_code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`borrow_details_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrowdetails`
--

LOCK TABLES `borrowdetails` WRITE;
/*!40000 ALTER TABLE `borrowdetails` DISABLE KEYS */;
INSERT INTO `borrowdetails` VALUES (25,5,1,'returned',NULL,NULL,'2017-03-11 22:10:25','aaa'),(26,5,2,'returned',NULL,NULL,'2017-03-12 12:16:21','aaa'),(27,6,5,'returned',NULL,NULL,'2017-03-12 13:33:23','E004015017997150');
/*!40000 ALTER TABLE `borrowdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id` (`id`),
  UNIQUE KEY `category_name` (`category_name`),
  KEY `classid` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Political Science'),(3,'测试');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('0085f6a009d0079fc7d74d5fad3162f54ab95cc9','101.199.108.120',1489264847,'__ci_last_regenerate|i:1489264847;'),('02c2ea8e6ca134eea291790cfe4bc506c2484b36','101.98.181.88',1489281006,'__ci_last_regenerate|i:1489281006;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('0362e3a5222241de3e967e1ddc42ea0aff267460','106.120.161.66',1492640884,'__ci_last_regenerate|i:1492640884;'),('050d7ba5837dddd352ff11f2139fb2edc22c6283','::1',1489225894,'__ci_last_regenerate|i:1489225893;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('07edf98ddde48db3a765f727b88d24a91721b304','::1',1489203076,'__ci_last_regenerate|i:1489203076;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('0852624a9646307a7fe356f6fa0df107415660d1','157.55.39.64',1492067480,'__ci_last_regenerate|i:1492067480;'),('0857ebc34214f2e66ec7ca8058729d7200c932c4','::1',1489221619,'__ci_last_regenerate|i:1489221358;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('09dc38b38f27bfd661bc8bddfc68ecd4d0faed82','220.181.132.216',1491511601,'__ci_last_regenerate|i:1491511601;'),('0a730e8b42045aa6e37a4e903fbdf630bb1cf49e','122.59.82.95',1491532211,'__ci_last_regenerate|i:1491532211;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491530018\";'),('0d3b86ca0fb1fedbbca95065e663c255bc1b898a','::1',1489203721,'__ci_last_regenerate|i:1489203462;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('0d919df8dfd92a4b0f2d82ed98ab8d99885f057a','101.199.108.54',1489264264,'__ci_last_regenerate|i:1489264264;'),('0df1f92b31de1f8e53edd8b3bc0c58cf0465559c','27.252.184.84',1491513074,'__ci_last_regenerate|i:1491513074;'),('0e424b1eb722113246755fb065459fb095f0f913','101.98.181.88',1489394409,'__ci_last_regenerate|i:1489394195;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489278183\";'),('107c7a17dcc877c3cd9abd9cf2d15f646aaf9a07','222.152.163.47',1491474782,'__ci_last_regenerate|i:1491474782;'),('1108a9c1bcd93c7ca4223a90c5fb92dc4c10b260','101.199.112.51',1489264410,'__ci_last_regenerate|i:1489264410;'),('111f287533570908cc6d6585eff4a7be6a88e528','::1',1489200471,'__ci_last_regenerate|i:1489200220;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('11835ffa7351f27e9dbeb05651d11b2a0ab340cf','::1',1489191024,'__ci_last_regenerate|i:1489190776;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('156a4415e044fed4324b657f9939391cb7f0340b','27.252.187.18',1489279746,'__ci_last_regenerate|i:1489279746;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('15d5fade6bde15ba0074909a8946b7778ba87cac','27.252.161.135',1490409092,'__ci_last_regenerate|i:1490409092;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489394208\";csrfkey|s:8:\"Zlbr3MaF\";__ci_vars|a:2:{s:7:\"csrfkey\";s:3:\"new\";s:9:\"csrfvalue\";s:3:\"new\";}csrfvalue|s:20:\"bzoSBO19sMm7JW2FpQ0c\";'),('15e66d01fea78d87dfc75c06995a7bf110fa590d','27.252.187.18',1489384633,'__ci_last_regenerate|i:1489384633;'),('16cb5fa8b86c9c9ddfb44f5d3cfd61470b780fdb','66.249.71.154',1491631503,'__ci_last_regenerate|i:1491631503;'),('1775974e6e335094fd217212bfe5c0dc2e49847a','66.249.69.89',1492068904,'__ci_last_regenerate|i:1492068904;'),('18a37b964b4b7565e8843883352cdaeb4ff01b47','::1',1489194087,'__ci_last_regenerate|i:1489193819;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('1b5e47b6aa5b76ed0e4a452d378666552887b02b','180.76.15.9',1492344974,'__ci_last_regenerate|i:1492344974;'),('1dd06d8c845bcc27a1765999e3e177f33803fd31','66.249.79.129',1491631503,'__ci_last_regenerate|i:1491631503;'),('2178e8547f5cf2e6e487b23143034a548f5e96d4','101.98.181.88',1489280197,'__ci_last_regenerate|i:1489280197;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489264416\";'),('21e22db854fe61f32701d7dc3133d42f18552fff','::1',1489223426,'__ci_last_regenerate|i:1489223136;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('22234a94d449defd09e51c4fbf112a29cf80161b','220.181.132.216',1491511590,'__ci_last_regenerate|i:1491511590;'),('2247d6f2c12b95d0d0a3d94f1542b019d96e55fa','::1',1489204036,'__ci_last_regenerate|i:1489203767;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('27309f530ba0d9ca468ea989f44401b4a2bfc556','::1',1489199859,'__ci_last_regenerate|i:1489199548;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('2760eff70f80c07b427daf0e6bcd0d5a0bfb71db','27.252.187.18',1489282476,'__ci_last_regenerate|i:1489282373;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('280b8e2533fcffc0c15bd18f22d27fe7e7290c11','222.152.163.47',1491473462,'__ci_last_regenerate|i:1491473462;'),('281ecc658e15de8dd029900e605cabd5d1c38238','222.152.163.47',1491476587,'__ci_last_regenerate|i:1491476587;'),('29b3e2589c1f9e05a8f67fe72ac3d418abe2a30e','::1',1489146567,'__ci_last_regenerate|i:1489146553;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489113598\";'),('29f764a2fdc53ee80f60454688437fbda0f522b7','180.76.15.19',1492344859,'__ci_last_regenerate|i:1492344859;'),('2d0055b57ffd4f711aa584ecaf3ffb5035b95db9','::1',1489222307,'__ci_last_regenerate|i:1489222206;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('2f04ebe8f71978178bf95402e76176fd00b81eff','220.181.132.216',1489264254,'__ci_last_regenerate|i:1489264254;'),('302956480fb015b280aab3cd36e92e230b77907b','122.59.82.95',1491542089,'__ci_last_regenerate|i:1491542089;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491531554\";'),('3496c480a87a705a26cad1a3b5c96e95d3bc0854','101.98.181.88',1489229217,'__ci_last_regenerate|i:1489229170;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489219155\";'),('34a246d82eb4c8a237b9012bb460359d131e2291','122.59.82.95',1491543991,'__ci_last_regenerate|i:1491543991;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491531554\";'),('34ade6f86ea99d368b8b7696375435432d2a9a20','220.181.132.216',1489264298,'__ci_last_regenerate|i:1489264298;'),('34adf6ac0e947e2adcad1de8433b6388416e1108','66.249.77.6',1492210173,'__ci_last_regenerate|i:1492210173;'),('34c950f5f3a73784f09cc79cd1cbfa09cfaafcbf','101.98.181.88',1489314812,'__ci_last_regenerate|i:1489314812;'),('3534ad7ff5a47dfcc0f9c20a499e109f38e7f9ca','27.252.161.135',1489607389,'__ci_last_regenerate|i:1489607389;'),('35c9c72c38cd14cef385c400b93cc4a320dd985f','122.59.82.95',1491531521,'__ci_last_regenerate|i:1491531500;'),('36b8515a541410cb635a836143f54c7debf4aa76','222.152.163.47',1491475939,'__ci_last_regenerate|i:1491475939;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('378d84e746ee76a97e8b820ee0c14755240ec0b8','220.181.132.216',1489264234,'__ci_last_regenerate|i:1489264234;'),('37df1b299f57856948518d5c2f65a2177d591a08','27.252.187.18',1489279439,'__ci_last_regenerate|i:1489279439;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('3a5d9068437a5e2f51ffe7d403deec2dbc986146','27.252.184.84',1491688044,'__ci_last_regenerate|i:1491688044;'),('3b939afae2a4689681aec26080a82ca4ca6bf082','222.152.163.47',1491475620,'__ci_last_regenerate|i:1491475620;'),('3d0953cbf490bafd495d55b9b01218405561459e','220.181.132.196',1489264201,'__ci_last_regenerate|i:1489264201;'),('3d390f72e1a62751027245a73ea81ef331581e95','101.98.181.88',1489280389,'__ci_last_regenerate|i:1489280197;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489264416\";'),('3d4b8b6f9741e8da25259b71cffd9bb19428c18c','66.249.77.4',1492607196,'__ci_last_regenerate|i:1492607196;'),('3dfec16c69a4bb5bc1ed5d3509ceaf786716b7c7','101.98.181.88',1489273871,'__ci_last_regenerate|i:1489273871;'),('3e0a9aeba73c20d73407e4602d4342c2438b8174','27.252.184.84',1491529983,'__ci_last_regenerate|i:1491529832;'),('3fc3e11055e3587ab71465c61e329267bfeab5a7','222.152.163.47',1491479213,'__ci_last_regenerate|i:1491479213;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1491473306\";'),('402f2e3e18624ad8470a207b535c4b9b1b97fe77','::1',1489195828,'__ci_last_regenerate|i:1489195811;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('40600752f6b715a14918ae058b64147093809fef','::1',1489195344,'__ci_last_regenerate|i:1489194781;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('42a6538759ff04fd8ad52139e4270c30999a2348','66.249.69.93',1492110464,'__ci_last_regenerate|i:1492110464;'),('42d0c4b9e3805e8aa6bafb09d5a46f776520dfb0','220.181.132.216',1489264190,'__ci_last_regenerate|i:1489264190;'),('44f503a4aafa59b09c8551be82b5a50191673706','::1',1489200474,'__ci_last_regenerate|i:1489200183;'),('454c4d21e799539284c507121f5800e6256be508','::1',1489200910,'__ci_last_regenerate|i:1489200608;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('460d5ee238761b6b5f80ee4da3bf41e8afb0ff62','101.98.181.88',1489282173,'__ci_last_regenerate|i:1489282173;'),('46925de410227a1693f2c7658d4646e1b42a94d7','220.181.132.197',1489264310,'__ci_last_regenerate|i:1489264310;'),('4b53f7bd52a039251cd3f608595188be7820a732','122.59.82.95',1491532569,'__ci_last_regenerate|i:1491532569;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491530018\";'),('4bd098b9e112625f8d19d87aee08c0644c84390a','27.252.187.18',1489268178,'__ci_last_regenerate|i:1489268178;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489229179\";'),('4bdc5e940183d5c3b334cb636cb985217e85792a','27.252.187.18',1489278165,'__ci_last_regenerate|i:1489278165;'),('4bee7c7a88e894cbdf07f840e28fee6e70dc0bca','119.188.70.31',1489264876,'__ci_last_regenerate|i:1489264876;'),('4cbe2caed0cab6ea1dc386994326a54cadf1fab8','101.98.181.88',1489280190,'__ci_last_regenerate|i:1489280189;'),('4d406ba8969f67bda02bd5d8c646cad5a8f3368b','101.98.181.88',1489279133,'__ci_last_regenerate|i:1489279133;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('4d44f9f5ab90c6aa6ef4df7b9a92e86388b45e76','27.252.187.18',1489280507,'__ci_last_regenerate|i:1489280507;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('4fc7b2ea571e03b7164db85e79ae4ea4149c2da5','101.98.181.88',1489278318,'__ci_last_regenerate|i:1489278318;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('50ce668618416440f7e49d36ccf105b84703e8fe','66.249.69.89',1492110465,'__ci_last_regenerate|i:1492110465;'),('50dba1a44f6ac47f6b8b27bb74240b7981f5a3b8','220.181.132.216',1490408518,'__ci_last_regenerate|i:1490408518;'),('50ea7073a35512248a12658713dcd46e5e17cfc5','27.252.187.18',1489280056,'__ci_last_regenerate|i:1489280056;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('5226bcf12778d59b578744623cd57361fc26051d','66.249.79.191',1491595696,'__ci_last_regenerate|i:1491595696;'),('529ed6d0596be691df970fb074a3eb57b4f6bd88','122.59.82.95',1491544025,'__ci_last_regenerate|i:1491543991;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491531554\";'),('53fda68e3ff6cc443d3e96a2c7585cbf8d12003c','::1',1489146425,'__ci_last_regenerate|i:1489146161;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489113598\";'),('544a48935299f2c58015b4a8e59d62a43ff9abf6','101.98.181.88',1489394305,'__ci_last_regenerate|i:1489394305;'),('5488b095f139bffda191225badd4f889f7962e89','::1',1489220063,'__ci_last_regenerate|i:1489219949;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('569f21c02354679cc3bf14030d23317658975c81','222.152.163.47',1491476256,'__ci_last_regenerate|i:1491476256;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('56f7511611e2b62e14ad1a9dceb5077f258c3f1d','::1',1489147054,'__ci_last_regenerate|i:1489147052;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489146163\";'),('5817b0107c2442e428ece741946eb1c2e791b1bc','27.252.161.135',1490409041,'__ci_last_regenerate|i:1490408869;'),('5a89a4957a8bd2df6050476cb71741fee43ef6d8','27.252.187.18',1489305698,'__ci_last_regenerate|i:1489305698;'),('5b7dbc59bee5c3de82aeae6e90ce22afdfe6e9cc','101.98.181.88',1489284296,'__ci_last_regenerate|i:1489284296;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";csrfkey|s:8:\"vlL9oKeA\";__ci_vars|a:2:{s:7:\"csrfkey\";s:3:\"new\";s:9:\"csrfvalue\";s:3:\"new\";}csrfvalue|s:20:\"XWEnr9B8Vea1TbDxYmMk\";'),('5c4d4fe969736757fe83c2aef1e56072106cfffb','66.249.71.155',1491595186,'__ci_last_regenerate|i:1491595186;'),('5c7e3716e703c7c29dbde55369178a3b63a82ad7','27.252.161.135',1490409195,'__ci_last_regenerate|i:1490409092;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489394208\";csrfkey|s:8:\"XABlRVJE\";__ci_vars|a:2:{s:7:\"csrfkey\";s:3:\"new\";s:9:\"csrfvalue\";s:3:\"new\";}csrfvalue|s:20:\"Q2wZBfLWKYME34acPbHR\";'),('5d6ba30264736ccade4d8b379e561f66e243cbe9','122.59.82.95',1491532869,'__ci_last_regenerate|i:1491532569;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491530018\";'),('5dfde5fd6c987f68d6f7a45a019b03bdf5e57507','222.152.163.47',1491477308,'__ci_last_regenerate|i:1491477308;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('5e30097e6e4bc4dca8e4dd8a29b903eacc688ab3','180.76.15.150',1492344859,'__ci_last_regenerate|i:1492344859;'),('5e497dc05b8c254dfcdf233f980a1cbb888685ef','101.98.181.88',1489278666,'__ci_last_regenerate|i:1489278666;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('6232db59ab6e24d5b1e2d02ea4d5e0181c0d697a','::1',1489227404,'__ci_last_regenerate|i:1489227361;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('62d4c1e3fae22f035d3812a9f2d94737f7daf4f5','27.252.187.38',1492640834,'__ci_last_regenerate|i:1492640834;'),('6348beed13a53afc66ff9aaa96e4070684a0a0ad','66.249.71.155',1491677670,'__ci_last_regenerate|i:1491677670;'),('63a2c159f602e6b9378a0ab1cace3d902e5e9a93','222.152.163.47',1491475939,'__ci_last_regenerate|i:1491475939;'),('6722bd0bf5c37b514a8fa61af0a2b5aa207bed79','222.152.163.47',1491478074,'__ci_last_regenerate|i:1491478074;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('6858d200fc6cb03e7520eca26b7c7fcb45f4009a','27.252.184.84',1491530046,'__ci_last_regenerate|i:1491529916;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491511696\";'),('687c83bd1a59abc8e05c682e72e95ba6abdb5362','27.252.184.84',1491513074,'__ci_last_regenerate|i:1491513074;'),('6952d975509ad130df3a43fe2566f493a1f82d52','::1',1489225590,'__ci_last_regenerate|i:1489225496;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('6aacb70d6bcd3c2b2d4d5ec6b938118e5e733958','::1',1489202953,'__ci_last_regenerate|i:1489202657;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('6b207c20b64377d314dac69e7ba69033f46b9176','222.152.163.47',1491479981,'__ci_last_regenerate|i:1491479981;message|s:72:\"<div class=\"alert alert-info\" role=\"alert\">You are now logged out!</div>\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('6b45ca4629783f02a87187841b3ebb435b8265f8','::1',1489199319,'__ci_last_regenerate|i:1489199028;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('6bf49c6d5842d8cc30fcb1b1946159f0e33ab0ef','220.181.132.216',1491529957,'__ci_last_regenerate|i:1491529957;'),('6c23d3745206ed4a0981eb92e611cd9c50556e8a','222.152.163.47',1491476256,'__ci_last_regenerate|i:1491476256;'),('70db959c7150bfba34ef8d3fd81221435f1b8bea','::1',1489199516,'__ci_last_regenerate|i:1489199426;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('72e0a22a0419dd523f46f04b1ce39e654d10da51','222.152.163.47',1491479196,'__ci_last_regenerate|i:1491479196;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";csrfkey|s:8:\"5OhuTd1H\";__ci_vars|a:2:{s:7:\"csrfkey\";s:3:\"new\";s:9:\"csrfvalue\";s:3:\"new\";}csrfvalue|s:20:\"blQYFnxNa7Ztc58fVsrU\";'),('7596c09221189c56894cbcc1527aab4a8835addc','66.249.79.188',1491594894,'__ci_last_regenerate|i:1491594894;'),('760aeaf1380df146e4cdd2af0cdf141dd6deb42c','101.98.181.88',1489281794,'__ci_last_regenerate|i:1489281794;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('78923633dbac850241639e22b19b89414daba8fb','222.152.163.47',1491476587,'__ci_last_regenerate|i:1491476587;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('7ad7a508114fb39292f0faeae21c521bbd4e3d28','101.98.181.88',1489273845,'__ci_last_regenerate|i:1489273845;'),('7bcd6b6412108d797188a9a19883d14beafe9ec6','::1',1489225071,'__ci_last_regenerate|i:1489224895;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('7cb83ec3933f043613d0d1ff644fbd534abb908a','220.181.132.197',1490408538,'__ci_last_regenerate|i:1490408538;'),('7d8d577af947db873c93b6e63232c2c321d32aa5','101.98.181.88',1489284297,'__ci_last_regenerate|i:1489284297;'),('7ea6b8f4aeda37673eed37e1d28840c220881219','101.98.181.88',1489269919,'__ci_last_regenerate|i:1489269884;'),('7efcd5d9b19bdbbb760667b329f09e6557eb3944','27.252.187.18',1489264145,'__ci_last_regenerate|i:1489264145;'),('7fc9fed6b04af7c5f8f8729f742463d8d6b9d8bb','222.152.163.47',1491479947,'__ci_last_regenerate|i:1491479947;'),('7fde9e65b278259396e4eb96d65c5312eef6630a','::1',1489192211,'__ci_last_regenerate|i:1489191997;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('80af1b408cdf4efdf8a83dae93b52fe29e95fd34','222.152.163.47',1491478074,'__ci_last_regenerate|i:1491478074;'),('82b2dba04c4b1177bbc4cf6661199197194c3dee','27.252.187.18',1489278098,'__ci_last_regenerate|i:1489278098;'),('8396ba284372ffb0778a37af56e9ad502b85af21','::1',1489195810,'__ci_last_regenerate|i:1489195510;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('8419bca6b44c7ac22409f9652e0488063672ef19','::1',1489223707,'__ci_last_regenerate|i:1489223453;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('84fdb6f4315832955d7476f18675b37057de2fd7','66.249.71.156',1491677669,'__ci_last_regenerate|i:1491677669;'),('8565e35587f4eddd1f90ec5ddd3c6f5a1dd15faa','66.249.77.4',1492586672,'__ci_last_regenerate|i:1492586672;'),('85ae800e27dea60459d64176a5d93fd31a94285e','103.76.205.33',1489374570,'__ci_last_regenerate|i:1489374570;'),('87bc9c0d1bb99163b501eeea914000a4a8a1666e','66.249.66.12',1491975290,'__ci_last_regenerate|i:1491975290;'),('8940b2c18383185689340bad6d20da5d36881258','222.152.163.47',1491479952,'__ci_last_regenerate|i:1491479947;'),('89d50f41481e28f6c8975c9714a8472e47ddd509','222.152.163.47',1491478871,'__ci_last_regenerate|i:1491478871;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('8a6f7a5ecc2336fe8b3b50bbb653044f38ea2346','27.252.187.18',1489282373,'__ci_last_regenerate|i:1489282373;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('8c2f142dc1c3ad4b28adcd2dedd9583ad7688306','222.152.163.47',1491478559,'__ci_last_regenerate|i:1491478559;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}message|s:47:\"<p>Account Information Successfully Updated</p>\";'),('8c489da4bace237759e0466763663ccce757511d','222.152.163.47',1491479952,'__ci_last_regenerate|i:1491479952;message|s:72:\"<div class=\"alert alert-info\" role=\"alert\">You are now logged out!</div>\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('8f65af41037fc213e59cbaf8bc8e05ad339047da','::1',1489222812,'__ci_last_regenerate|i:1489222605;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('8f88c7701fc2c4d6b37fa72ea048dd960525b553','101.98.181.88',1489229173,'__ci_last_regenerate|i:1489229173;'),('903aa4920697b8e2cc6c90e2a95b0cc54518da38','27.252.187.18',1489278176,'__ci_last_regenerate|i:1489278176;'),('923315c536f565060f2a9f2c85298ede14b2c6bc','::1',1489198984,'__ci_last_regenerate|i:1489198671;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('9387af86a6b5f6843ffd8a0bee9d2372d90b2ede','::1',1489204267,'__ci_last_regenerate|i:1489204139;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('942bf7cc088b752521f9680d84bcf18469ca2cae','27.252.161.135',1490743675,'__ci_last_regenerate|i:1490743675;'),('947c8839027967a626eb4848fd2a762ea1cf437e','220.181.132.216',1490408512,'__ci_last_regenerate|i:1490408512;'),('94baeb5e0264435396d5b93a009d243099bc8c8f','27.252.161.135',1489466115,'__ci_last_regenerate|i:1489466115;'),('96cc7b4f8c272f5e7c81ba67e65537ec5f4a4f4f','66.249.79.188',1491594894,'__ci_last_regenerate|i:1491594894;'),('96fbba7c83c4c5eee6bfbbd609e28d1ca88d498d','27.252.187.18',1489278432,'__ci_last_regenerate|i:1489278432;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('974996099a7bbe4c0c6311c22ec5e354b14a9832','222.152.163.47',1491476954,'__ci_last_regenerate|i:1491476954;'),('98489a1f04e05b23b4d369ed04a08253eb015cc7','27.252.161.135',1490409273,'__ci_last_regenerate|i:1490409233;'),('98aed0268502220e63cf2ca88ff2bc5fe8696cb6','::1',1489191103,'__ci_last_regenerate|i:1489191083;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('9ac483835d2b10a236bc76d128b3050e7feacc58','101.98.181.88',1489284297,'__ci_last_regenerate|i:1489284297;'),('9b72a79f28f929f0371c73da2488abdb26b28716','222.152.163.47',1491478891,'__ci_last_regenerate|i:1491478891;'),('9bce5e8e51efc7c33e481df18c34d6f10ba78db4','27.252.161.135',1490427401,'__ci_last_regenerate|i:1490427401;'),('9d3c6e5d6da629cf709946b38cef6e6ba8608868','220.181.132.216',1489264394,'__ci_last_regenerate|i:1489264394;'),('9d3da4cc442f8eec7a2f3b3f09e862575b77d700','192.162.227.249',1491622233,'__ci_last_regenerate|i:1491622233;'),('9d57b5eb8125dbf1b8dd4c522d1d12f091c9474f','27.252.187.18',1489265165,'__ci_last_regenerate|i:1489265165;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489229179\";'),('a26cb4779cee2a43e017b05e83474ad929371a2e','27.252.161.135',1489522418,'__ci_last_regenerate|i:1489522418;'),('a34177ab67250e983027589b8ea53b9af4eb176e','222.152.163.47',1491474169,'__ci_last_regenerate|i:1491474169;'),('a55561ccb079870f64f6f0bbcc48acea8c6cb951','101.199.108.55',1489264243,'__ci_last_regenerate|i:1489264243;'),('a74b35eee6712e038f85fea6ee0963e607700deb','::1',1489221993,'__ci_last_regenerate|i:1489221661;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('a86d448ab2bfc75cc6c49caf02584203b95a753b','222.152.163.47',1491479498,'__ci_last_regenerate|i:1491479498;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('a9c2a472102dd317ed4f5216a17efc5eed1e2964','27.252.161.135',1490408460,'__ci_last_regenerate|i:1490408460;'),('aaa41e5d6741b57f18beadf2a67bce0d08a43cfa','::1',1489202429,'__ci_last_regenerate|i:1489202161;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('ac3937dbd9258a736e36ff9676d74dd2a633247d','::1',1489194515,'__ci_last_regenerate|i:1489194442;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('ac78178f71d1f2f5bb216d809bb99dfa82ccc4c0','101.98.181.88',1489284297,'__ci_last_regenerate|i:1489284296;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}message|s:47:\"<p>Account Information Successfully Updated</p>\";'),('af1a743e33b63b167eced879a13444af3190979d','66.249.79.188',1491631756,'__ci_last_regenerate|i:1491631756;'),('b222a458f8a2b74274e9b7ea5d64a3e8b8bdb162','220.181.132.216',1491529889,'__ci_last_regenerate|i:1491529889;'),('b40d572c59225f83ae43e149191c8aff4202c8cb','27.252.187.18',1489278118,'__ci_last_regenerate|i:1489278118;'),('b48a6c2764f444c5085d006b77b8617d1618733a','101.98.181.88',1489282155,'__ci_last_regenerate|i:1489282155;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}message|s:47:\"<p>Account Information Successfully Updated</p>\";'),('b50a5ec03cca57953d00ec3dc894a3ad0c9742c5','222.152.163.47',1491473060,'__ci_last_regenerate|i:1491473060;'),('b579edaf36f8081abbff9d69e89cf1149ebc3e7f','::1',1489202090,'__ci_last_regenerate|i:1489201796;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('b6d54c56d48ee2135f9d949aa2f7ee12e1eda831','66.249.77.6',1492586672,'__ci_last_regenerate|i:1492586672;'),('b6faa365b4a4c569dcad185b2629389d002746e2','68.180.231.46',1491581066,'__ci_last_regenerate|i:1491581066;'),('b83714768cb3b12a44d2ef97cf19e7751bc137b9','101.98.181.88',1489274202,'__ci_last_regenerate|i:1489274202;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489264416\";'),('b94a5fd6737a47bdd0d7b17c6c915eeb5839126b','101.199.112.54',1491511608,'__ci_last_regenerate|i:1491511608;'),('b94e93bb8024b78c67a5cc9f1b6e090c72c4c8c9','101.199.108.51',1491529908,'__ci_last_regenerate|i:1491529908;'),('b97262c0d9e7e551f611cafbc9102a11cade08e6','103.76.205.33',1489374570,'__ci_last_regenerate|i:1489374570;'),('bb25f732dd4b37cb12fadc8957d1dfc760a0b27e','27.252.187.18',1489278108,'__ci_last_regenerate|i:1489278108;'),('bbddfd2c77cc0e6fc36d343172ef4b24914363ed','220.181.132.216',1489264239,'__ci_last_regenerate|i:1489264239;'),('bbeaa55f22e04200faf57dacbf73970fe397b25d','::1',1489226598,'__ci_last_regenerate|i:1489226445;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('bfa9f30122563f952bc1fb6d2cb5d434c907fbdc','::1',1489220542,'__ci_last_regenerate|i:1489220313;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('bfefe223674615e953901507dda50eaad2eab693','182.118.20.143',1489264326,'__ci_last_regenerate|i:1489264326;'),('c012da2056501fe1ab0a0f3f076964962f1ef64c','101.98.181.88',1489281377,'__ci_last_regenerate|i:1489281377;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";csrfkey|s:8:\"WYTsAXm4\";__ci_vars|a:2:{s:7:\"csrfkey\";s:3:\"old\";s:9:\"csrfvalue\";s:3:\"old\";}csrfvalue|s:20:\"NApHoieSmhaxr2R5481n\";'),('c0cdbc5f68114d5aa1e5b47592a0431096d184ab','::1',1489200205,'__ci_last_regenerate|i:1489199880;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489190420\";'),('c116d692bec5177a9d70aab865d4861577be94f9','192.162.227.249',1491622235,'__ci_last_regenerate|i:1491622235;'),('c1fb1066e94f890dea7130931a33fb42e7fbff57','222.152.163.47',1491475613,'__ci_last_regenerate|i:1491475613;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('c26a6d7b003aa6cb6fdb9302f4af7a3dee9480e0','27.252.161.135',1490408788,'__ci_last_regenerate|i:1490408788;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489394208\";'),('c38ce91ad14a3179ec95a53cd6449b084f72b618','::1',1489190425,'__ci_last_regenerate|i:1489190418;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('c3f57c1beb151a74e439c92ebb000798fe3ec9a4','27.252.187.18',1489278142,'__ci_last_regenerate|i:1489278142;'),('c45859db45c75ad5d64862e0a3537b5a15753c3f','::1',1489197869,'__ci_last_regenerate|i:1489197491;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('c52df1094fe7ad5d3d7600a9a4fdde4580df6e68','::1',1489223957,'__ci_last_regenerate|i:1489223766;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('c5cf2d7eb5c664c2928f47f7405b0f8aa1a2d372','27.252.161.135',1489607389,'__ci_last_regenerate|i:1489607389;'),('c6cfe197b9e49c64561dc2c870a764fdb8691b5a','::1',1489200908,'__ci_last_regenerate|i:1489200610;'),('c6e710b113cdef2ccc245453bdf8535eafd45d3e','27.252.184.84',1491511862,'__ci_last_regenerate|i:1491511567;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473481\";'),('c74cf4204825f65173c1128ec59b19f20f45c518','101.98.181.88',1489699667,'__ci_last_regenerate|i:1489699667;'),('c83a6e16767fe95b0e2bf307e433221b3c7193b3','101.199.108.59',1491529972,'__ci_last_regenerate|i:1491529972;'),('c9620c830268f307f041d3723450395533c9e071','::1',1489194407,'__ci_last_regenerate|i:1489194141;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('c9a27a67a755276f7f710c8286a1fc01ecf528a9','122.59.82.95',1491541785,'__ci_last_regenerate|i:1491541785;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491531554\";'),('ca6d443fdc5ca7777cf8226b6fcae47634818fad','220.181.132.216',1489264282,'__ci_last_regenerate|i:1489264282;'),('caaa858b1b5e4c2a3d3259d44d280804c9844adf','222.152.163.47',1491477719,'__ci_last_regenerate|i:1491477719;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('cab1b6be72b853f4e1cca842257b4be524bfdcf6','27.252.161.135',1489466115,'__ci_last_regenerate|i:1489466115;'),('cb53dd7e50a22abc3bba642aa11ccda36947212f','222.152.163.47',1491474044,'__ci_last_regenerate|i:1491474044;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('cb811cdfdcbca5f58e354f43b23e3ed9484d9fd0','::1',1489147588,'__ci_last_regenerate|i:1489147372;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489146163\";'),('cc213925f61a311d6dbc61d6c9d3c6ce2c993623','::1',1489198200,'__ci_last_regenerate|i:1489197895;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('ccead3aa02eebc21e8398e5946df7aa016c74492','222.152.163.47',1491477308,'__ci_last_regenerate|i:1491477308;'),('d01aab13646ca7dccf1c3a1533595407e37f3699','101.98.181.88',1489280699,'__ci_last_regenerate|i:1489280699;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('d340fb322d8a73037706c83fc9719982893d0d3d','103.76.205.33',1489377711,'__ci_last_regenerate|i:1489377711;'),('d54f6d1beada0a169b56f4e4b6fa59f52b2531f5','66.249.66.6',1491975291,'__ci_last_regenerate|i:1491975291;'),('d56468b07562b15fcba1ccb88af2ced85ab7745d','::1',1489201412,'__ci_last_regenerate|i:1489201112;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('da1c5c0d97d29e3739a27af70d3bf04a59d004fd','66.249.71.155',1491595499,'__ci_last_regenerate|i:1491595499;'),('da498518a9c90670f7f130fdf4423179aa526078','101.199.108.53',1490408527,'__ci_last_regenerate|i:1490408527;'),('db4e5f684108e3c95ab528ebcc60053759a30055','68.180.231.46',1491584349,'__ci_last_regenerate|i:1491584349;'),('db5cade79847f4e7c782abf601d842a63f209bf7','27.252.187.18',1489264408,'__ci_last_regenerate|i:1489264145;'),('dc9d649ec4bf20d0ec01ae4d6db9608c13ebb4bb','27.252.161.135',1490409317,'__ci_last_regenerate|i:1490409215;identity|s:14:\"test@test.test\";email|s:14:\"test@test.test\";user_id|s:1:\"3\";old_last_login|N;'),('dcbead290ed9bd9f238b5b4bbfbef017843b22f9','101.199.108.59',1492640898,'__ci_last_regenerate|i:1492640898;'),('deeb517dcebf739ff8015f602fd10d1cfd75c7be','222.152.163.47',1491479244,'__ci_last_regenerate|i:1491479244;'),('e33710dca316e9942c7e8a00e346d2cab1bc4620','::1',1489200182,'__ci_last_regenerate|i:1489199882;'),('e45c45126a075053c862cf6c5d712c9aa9dd6c4d','::1',1489219850,'__ci_last_regenerate|i:1489219640;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('e54d157ba8958e7f1cde51dd1f7c9508aa4960e8','::1',1489219322,'__ci_last_regenerate|i:1489219147;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";message|s:35:\"You have successfully issued a Book\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('e656d23969bb6c26a73d52f6982cd679d08f3e41','122.59.82.95',1491531799,'__ci_last_regenerate|i:1491531799;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491530018\";'),('e709e4d0cbf92c87ac763f5d64ef8b9d464aee52','27.252.187.18',1489384633,'__ci_last_regenerate|i:1489384633;'),('e7a6e346d9d1d5ca0cc2d0622cf36ed210e09385','::1',1489201683,'__ci_last_regenerate|i:1489201465;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('e7c148f57d78459a67aa62494eb9ca53dfb18568','207.46.13.183',1492067487,'__ci_last_regenerate|i:1492067487;'),('e8b54c70291818514acc7dc7da850667ec4a1de7','220.181.132.197',1489264251,'__ci_last_regenerate|i:1489264251;'),('eab373f635f9f5ed7314972f69ce1c8ba50f13da','::1',1489220953,'__ci_last_regenerate|i:1489220706;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('ec86165e1095d53402c556a720a1620e07672b17','27.252.161.135',1489522418,'__ci_last_regenerate|i:1489522418;'),('ee34d71be59dc08a018224b4b5e0afd2ebecc268','66.249.65.32',1492607196,'__ci_last_regenerate|i:1492607196;'),('eeeca9cf36ebb263f2383ae3ac475295ddf24ad1','27.252.161.135',1490408869,'__ci_last_regenerate|i:1490408869;'),('ef32eee65660af6970fe6e26ef77d5a0fd685959','27.252.161.135',1490491487,'__ci_last_regenerate|i:1490491487;'),('ef73abfd0cbb287759caff147b791472196c3cc7','103.76.205.33',1489779372,'__ci_last_regenerate|i:1489779372;'),('f2d6d9e8532da798c6b772a30824fc5954e61e4b','::1',1489198649,'__ci_last_regenerate|i:1489198203;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('f359db307316c0369998f5bf64fd0be5a1913ba9','101.199.108.119',1491511601,'__ci_last_regenerate|i:1491511601;'),('f377dfbe9f5bf5f08a62ad6fba2c9a564fc82324','::1',1489199776,'__ci_last_regenerate|i:1489199544;'),('f431ef26327cfcf9e1e292f39f247aa12b64a385','222.152.163.47',1491477720,'__ci_last_regenerate|i:1491477720;'),('f4406a26af2970bb56508eb94b98db30cb4ce9fa','101.98.181.88',1489279496,'__ci_last_regenerate|i:1489279496;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('f4a17dad6ea71791ce070381d81c43ed21273ed9','27.252.187.18',1489268196,'__ci_last_regenerate|i:1489268178;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489229179\";'),('f7329e37c70074045ad5180facd018e0e5e74e01','27.252.161.135',1490427401,'__ci_last_regenerate|i:1490427401;'),('f7550f854b57e0ecab08a7eec636c12a3ac7428f','222.152.163.47',1491476954,'__ci_last_regenerate|i:1491476954;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('f7a31f0719ef960d7488ddfea3f4dc7cb07ad4a7','27.252.187.18',1489278777,'__ci_last_regenerate|i:1489278777;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('f9a4a2c236e3b4e47510cbadfefca03cec1fcdcd','101.98.181.88',1489274173,'__ci_last_regenerate|i:1489274173;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489269944\";'),('fafe21d8d1af0cbc969d3dd596885ed1ac919ef1','::1',1489191936,'__ci_last_regenerate|i:1489191643;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489147054\";'),('fc1dd69d35cc5ddcc64f2b7a63159d35442aa921','68.180.231.46',1491581065,'__ci_last_regenerate|i:1491581065;'),('fc3c21d7b0e1558dacc008a9d6cdbc7637cae326','27.252.187.18',1489266690,'__ci_last_regenerate|i:1489266690;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489229179\";'),('fc7397e1d1cde08e16d76dad275e25bda0330db0','::1',1489227232,'__ci_last_regenerate|i:1489226968;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489199557\";'),('fc9aa412e7ff27e9ebb6f071f2b60c5d9a256924','27.252.187.18',1489279118,'__ci_last_regenerate|i:1489279118;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489273877\";'),('fd508b1345b71eff9fc32f0e566727b08654c3ce','101.98.181.88',1489275614,'__ci_last_regenerate|i:1489275614;identity|s:24:\"admin@multisupport.co.nz\";email|s:24:\"admin@multisupport.co.nz\";user_id|s:1:\"1\";old_last_login|s:10:\"1489264416\";'),('fdb4478129e060ae3453399cb9f9daebe79b4c2d','222.152.163.47',1491474781,'__ci_last_regenerate|i:1491474781;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491473082\";'),('fdb46652fb87dcd1944cce780326986c1d0fdb1a','27.252.187.18',1489278131,'__ci_last_regenerate|i:1489278131;'),('ffd95c0e9357bfbc40495aeedc0d279132d2c03e','122.59.82.95',1491541416,'__ci_last_regenerate|i:1491541416;identity|s:27:\"admin@chineselibrary.org.nz\";email|s:27:\"admin@chineselibrary.org.nz\";user_id|s:1:\"4\";old_last_login|s:10:\"1491531554\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `type_id` int(5) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES (16,'馆长',6);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Sudoer Administrator'),(2,'members','General User'),(3,'staff','Staff');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_types`
--

DROP TABLE IF EXISTS `member_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `borrowertype` varchar(50) DEFAULT NULL,
  `fine` varchar(100) NOT NULL,
  `issue_limit_books` varchar(50) NOT NULL,
  `issue_limit_day` varchar(50) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_types`
--

LOCK TABLES `member_types` WRITE;
/*!40000 ALTER TABLE `member_types` DISABLE KEYS */;
INSERT INTO `member_types` VALUES (8,'未激活','','０','０'),(7,'普通会员','','10','5'),(6,'管理员','','2000','2222');
/*!40000 ALTER TABLE `member_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `books-index` tinyint(1) DEFAULT '0',
  `books-add` tinyint(1) DEFAULT '0',
  `books-edit` tinyint(1) DEFAULT '0',
  `books-delete` tinyint(1) DEFAULT '0',
  `books-read` tinyint(1) DEFAULT '0',
  `books-getBookDetails` tinyint(1) DEFAULT '0',
  `books-import_csv` tinyint(1) DEFAULT '0',
  `book-print_barcodes` tinyint(1) NOT NULL DEFAULT '0',
  `settings-index` tinyint(1) DEFAULT '0',
  `issued-index` tinyint(1) DEFAULT '0',
  `books-categories` tinyint(1) DEFAULT '0',
  `books-authors` tinyint(1) DEFAULT '0',
  `borrow-index` tinyint(1) DEFAULT '0',
  `borrow-bookreturn` tinyint(1) DEFAULT '0',
  `borrow-borrowed` tinyint(1) DEFAULT '0',
  `settings-sms` tinyint(1) DEFAULT '0',
  `settings-list_db` tinyint(1) DEFAULT '0',
  `settings-backup_db` tinyint(1) DEFAULT '0',
  `settings-restore_db` tinyint(1) DEFAULT '0',
  `settings-remove_db` tinyint(1) DEFAULT '0',
  `auth-index` tinyint(1) DEFAULT '0',
  `auth-create_user` tinyint(1) DEFAULT '0',
  `auth-groups` tinyint(1) DEFAULT '0',
  `auth-edit_group` tinyint(1) DEFAULT '0',
  `auth-member_types` tinyint(1) DEFAULT '0',
  `auth-occupations` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-quick_inventory` tinyint(1) DEFAULT '0',
  `delayed-index` tinyint(1) NOT NULL DEFAULT '0',
  `delayed-due_books_json` tinyint(1) NOT NULL DEFAULT '0',
  `delayed-email_templates` tinyint(1) NOT NULL DEFAULT '0',
  `delayed-notify_delayed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,2,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `section_id` int(50) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,'A'),(2,'B'),(3,'C'),(4,'D'),(5,'E'),(6,'F'),(7,'G'),(8,'H'),(9,'I'),(10,'J'),(11,'K'),(12,'L'),(13,'M'),(14,'N'),(15,'O'),(16,'P'),(17,'Q'),(18,'R'),(19,'S'),(20,'T'),(21,'U'),(22,'V'),(23,'W'),(24,'X'),(25,'Y'),(26,'Z');
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET latin1 NOT NULL,
  `issue_conf` int(1) NOT NULL COMMENT 'By Member Types: 2, System-Wide:1',
  `fine` int(10) NOT NULL,
  `issue_limit_days` int(10) NOT NULL,
  `issue_limit_books` int(10) NOT NULL,
  `language` varchar(100) NOT NULL DEFAULT 'english',
  `toggle_rtl` tinyint(1) NOT NULL DEFAULT '0',
  `currency` varchar(10) NOT NULL DEFAULT '',
  `email` varchar(30) NOT NULL DEFAULT '',
  `logo` varchar(50) NOT NULL DEFAULT '',
  `favicon` varchar(40) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL DEFAULT '',
  `title_small` varchar(10) NOT NULL DEFAULT '',
  `terms_conditions` text NOT NULL,
  `books_custom_fields` longtext NOT NULL,
  `smtp_host` varchar(100) NOT NULL DEFAULT '',
  `smtp_user` varchar(100) NOT NULL DEFAULT '',
  `smtp_pass` varchar(255) NOT NULL DEFAULT '',
  `smtp_port` varchar(10) NOT NULL DEFAULT '25',
  `version` decimal(6,2) DEFAULT NULL,
  `issue_limit_days_extendable` tinyint(1) NOT NULL,
  `notify_delayed_no_days_limit_toggle` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'Chinese Library Management System- Multi Support',2,50,10,10,'chinese',0,'NZD','info@multisupport.co.nz','45906a14e9c1bff095fb72be51a05f1f.png','LOGO.ico','panmure','0278226899','LMS','<ul>\r\n <li><strong>All copyright, trade marks, design rights,</strong> patents and other intellectual property rights (registered and unregistered) in and on LMS Online Services and LMS Content belong to the LMS and/or third parties (which may include you or other users.)</li>\r\n <li>The LMS reserves all of its rights in LMS Content and LMS Online Services. Nothing in the Terms grants you a right or licence to use any trade mark, design right or copyright owned or controlled by the LMS or any other third party except as expressly provided in the Terms.</li>\r\n</ul>','notes','ssl://smtp.gmail.com','2312456789@gmail.com','password','465',2.21,1,0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_settings`
--

DROP TABLE IF EXISTS `sms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_gateway` varchar(100) NOT NULL DEFAULT '',
  `auth_id` varchar(100) NOT NULL DEFAULT '',
  `auth_token` varchar(255) NOT NULL DEFAULT '',
  `api_id` varchar(100) NOT NULL DEFAULT '',
  `phone_number` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_settings`
--

LOCK TABLES `sms_settings` WRITE;
/*!40000 ALTER TABLE `sms_settings` DISABLE KEYS */;
INSERT INTO `sms_settings` VALUES (1,'nexmo','2132456','2345','12345','+123245');
/*!40000 ALTER TABLE `sms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `gender` enum('Male','Female','Other','') NOT NULL,
  `member_unique_id` varchar(20) NOT NULL,
  `type_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(2) NOT NULL,
  `image` text NOT NULL,
  `address` text NOT NULL,
  `location` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@multisupport.co.nz',NULL,NULL,NULL,'/JLvbejX0W.RyVBcPWoiPe',1268889823,1491478908,1,'Admin','Admin','ADMIN','0','Male','14914730389508',6,0,'A','no_image.png','afs','1'),(4,'222.152.163.47','chineselibrary','$2y$08$t5eXsVswsFAe8c8jrXhSVuTYUqGcjUjZRs7F7XTtXXHbKr2ogKZWq',NULL,'admin@chineselibrary.org.nz',NULL,NULL,NULL,'D3lVGZeIikaz1Sk9BenwO.',1491472983,1491540351,1,'Chinese','Library','chineselibrary','123456','Male','149147335311148',6,16,'','no_image.png','Panmure','1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (17,1,1),(20,4,3);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-20  0:15:31
